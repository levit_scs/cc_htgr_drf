# Levit's Django CookieCutter template for DRF apps

### Before getting started

This cookie cutter provides a Django project built according to the _project app_ architecture.

In short, with the _project app_ architecture, the almost empty directory named after your project becomes an *app* like any other app in your project. This has numerous advantages, the most noticeable of them being that you don't need to create a _core_ or _base_ app but can use your project app instead.

If you haven't seen [Anatomy of a Django Project by Mark Lavin](https://www.youtube.com/watch?v=ajEDo1semzs), I would emcourage you to do so to get a better grasp of what the _project app_ architecture is about.

### CookieCutter

For more info on CookieCutter, please visit [their documentation](http://cookiecutter.readthedocs.org/en/latest/)

### Usage

To use, simply run 
`cookiecutter https://bitbucket.org/levit_scs/cc_htgr_drf.git`

### Requirements:

- [CookieCutter](http://cookiecutter.readthedocs.org/en/latest/)

### Included in this CoookieCutter:

- [Django](https://www.djangoproject.com/)
- [Django Rest Framework](http://www.django-rest-framework.org/)
- [Django Filter](https://django-filter.readthedocs.org/en/latest/)
- [Django CORS headers](https://github.com/ottoyiu/django-cors-headers)
- [Celery](http://www.celeryproject.org/)
- [Django Debug Toolbar](http://django-debug-toolbar.readthedocs.org/en/1.4/) and [Django Debug Panel](https://github.com/recamshak/django-debug-panel)
- [Factory Boy](https://factoryboy.readthedocs.org/en/latest/)
- [DRF-Schema-Adapter](https://drf-schema-adapter.readthedocs.io/)

- A base test to test most standard `ModelViewSet`'s

### Notes:

1. Make sure your system allows you to run scripts from /tmp (this isn't the case for a default Ubuntu installation)

2. **This template currently only works on unix-based systems. PR for an MS Windows .bat hook or for converting the current hook to a python hook are welcome**

3. This template comes with a base test class to test most ModelViewSets, look at the `base.py` file in the `tests` folder for more info


---


This project is licensed under the [MIT License](http://opensource.org/licenses/MIT)

Before contributing, commenting or interacting with this project in any form, please, make sure you read and understand our [Code of Conduct](COC.md)
